<?php

namespace AmsomUtilities;

use Doctrine\Persistence\ManagerRegistry;
use Exception;
use PDO;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class OracleFunctionCalling
{
    private ManagerRegistry $managerRegistry;
    
    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    public function call($function, $params)
    {
        $SQLBlock = "
            declare
            res varchar2(32000);
            Begin
            :res := $function($params);
            commit;
            end;
            ";
        //    dd($SQLBlock);
        $res = null;
        try {
            $stmt = $this->managerRegistry->getConnection()->prepare($SQLBlock);
            $stmt->bindParam(':res', $res, PDO::PARAM_INPUT_OUTPUT, 255);
            //stock la réponse pour eventuel debug
            $result = $stmt->execute();
        } catch (Exception $e) {
            $result = false;
            throw new HttpException(500, $e->getMessage());
        }
        return $res;
    }

    public function largeCall($function, $paramsBeforeLarge, $largeParam, $paramsAfterLarge = '')
    {
        if ($paramsBeforeLarge !== '') {
            $paramsBeforeLarge .= ',';
        }
        if ($paramsAfterLarge !== '') {
            $paramsAfterLarge = ',' . $paramsAfterLarge;
        }
        $SQLBlock = "
            DECLARE
                v_clob clob:='';
                res varchar2(4000);
            BEGIN
            ";
        do {
            $SQLBlock .= "v_clob := v_clob || '" . substr($largeParam, 0, 32000) . "';";
            $largeParam = substr($largeParam, 32000, strlen($largeParam) - 32000);
        } while (strlen($largeParam) > 32000);
        $SQLBlock .= "v_clob := v_clob || '" . $largeParam . "';
                :res := $function(
                $paramsBeforeLarge
                v_clob
                $paramsAfterLarge
                );
            END;";
        $res = null;
        try {
            $stmt = $this->managerRegistry->getConnection()->prepare($SQLBlock);
            $stmt->bindParam(':res', $res, PDO::PARAM_INPUT_OUTPUT, 255);
            //stock la réponse pour eventuel debug
            $result = $stmt->execute();
        } catch (Exception $e) {
            $result = false;
            throw new HttpException(500, $e->getMessage());
        }
        return $res;
    }

    public function fileCall($function, $paramsBeforeFile, $file, $paramsAfterFile = '')
    {
        $errorFile = $this->fileSizeHandling($file);
        if ($errorFile) {
            return '{"error": "' . $errorFile . '"}';
        }
        $SQLBlock = "
            DECLARE
                v_clob clob:='';
                v_clob_decoded clob;
                v_blob blob;
                res date;
            BEGIN
            ";
        do {
            $SQLBlock .= "
                v_clob := v_clob || '" . substr($file, 0, 32000) . "';
                ";
            $file = substr($file, 32000, strlen($file) - 32000);
        } while (strlen($file) > 32000);
        $SQLBlock .= "v_clob := v_clob || '" . $file . "';
                v_clob_decoded := DecodeBASE64(v_clob);
                v_blob := ClobToImage(v_clob_decoded);
                :res := $function(
                $paramsBeforeFile,
                dbms_lob.getlength(v_blob),
                v_blob
                $paramsAfterFile
                );
            END;";
        $res = null;
        try {
            $stmt = $this->managerRegistry->getConnection()->prepare($SQLBlock);
            $stmt->bindParam(':res', $res, PDO::PARAM_INPUT_OUTPUT, 255);
            //stock la réponse pour eventuel debug
            $result = $stmt->execute();
        } catch (Exception $e) {
            $result = false;
            throw new HttpException(500, $e->getMessage());
        }
        return $res;
    }

    public function handleResult($res, $responseCode): Response
    {
        $json = json_decode($res);
        //Tout ajout retourne un json avec la props 'data'
        if (isset($json->data)) {
            $response = new Response(
                $res,
                $responseCode,
                [
                    'content-type' => 'application/json',
                    'Access-Control-Allow-Origin' => '*'
                ]
            );
        } else {
            //Renvoie l'erreur recu par le SQL
            $response = new Response(
                $res,
                Response::HTTP_BAD_REQUEST,
                [
                    'content-type' => 'application/json',
                    'Access-Control-Allow-Origin' => '*'
                ]
            );
        }
        return $response;
    }

    private function fileSizeHandling($file, $maxSize = null)
    {
        if ($maxSize === null) {
            $maxSize = $_ENV['FILE_MAX_SIZE'];
        }
        // x0.9 pour la tolérance
        $size = (strlen($file) / 4 * 3) * 0.9;
        if ($size > $maxSize) {
            return 'Le ou les fichiers excèdent la taille maximale admise de 10Mo';
        }
        return null;
    }
}
